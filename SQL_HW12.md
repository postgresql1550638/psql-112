// q1
select COUNT(*) from film
where length > (
select AVG(length) from film
)

// q2
select COUNT(*) from film
where rental_rate = (
select MAX(rental_rate) from film
)

// q3
select * from film
where 
rental_rate = (select min(rental_rate) from film) 
and 
replacement_cost = (select min(replacement_cost) from film)

// q4
SELECT payment.customer_id, COUNT(*)
FROM payment
INNER JOIN customer ON customer.customer_id = payment.customer_id
GROUP BY payment.customer_id
HAVING COUNT(*) = (
SELECT COUNT(*)
FROM payment
INNER JOIN customer ON customer.customer_id = payment.customer_id
GROUP BY payment.customer_id
ORDER BY COUNT(*) DESC
LIMIT 1
)